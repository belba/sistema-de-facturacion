import { Cliente } from './cliente';
import { ItemFactura } from './items-factura';

export class Factura {
  id: number;
  numeroFactura: string='';
  fechaFactura: string='';
  nombreEmpresa: string='';
  direccion: string='';
  codigoPostal: string='';
  ciudad: string='';
  pais: string = "España";
  telefono: string='';
  emailEmpresa: string='';
  webEmpresa: string='';
  subtotal: number = 0;
  iva: number = 0;
  totalPagar: number = 0;
  cliente = new Cliente();
}
