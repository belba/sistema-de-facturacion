import { from } from 'rxjs';
import { Factura } from './factura'

export class Cliente {
  id: number;
  nombreCliente: string='';
  direccion: string='';
  codigoPostal: string='';
  ciudad: string='';
  pais: string = "España";
}
