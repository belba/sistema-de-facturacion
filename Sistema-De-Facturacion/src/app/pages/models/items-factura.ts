export class ItemFactura {
  idItems: number;
  descripcion: string='';
  cantidad: number = 0;
  precio: number = 0;
  total: number = 0;
}
