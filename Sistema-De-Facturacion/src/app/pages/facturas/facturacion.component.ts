import { Component, OnInit, SystemJsNgModuleLoader, ViewContainerRef } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItemFactura } from '../models/items-factura';
import { Observable, of } from 'rxjs'

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Factura } from '../models/factura';
import { ToastrService } from 'ngx-toastr';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.scss']
})
export class FacturacionComponent implements OnInit {
  itemFacturaForms: FormArray = this.fb.array([]);
  itemsList: ItemFactura[] = new Array();
  factura: Factura;
  notificacion = null;
  constructor(private fb: FormBuilder, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.factura = new Factura();
    this.addItemFacturaForm();
  }

  addItemFacturaForm() {
    this.itemFacturaForms.push(this.fb.group({
      idItems: [0],
      descripcion: [''],
      cantidad: ['', Validators.required],
      precio: ['', Validators.required],
      total: [0]
    }));
  }

  recordSubmit(fg: FormGroup, i: number) {
    if (fg.value.idItems == 0) {
      fg.value.idItems = i;
      this.factura.subtotal += parseFloat(fg.value.total);
      this.factura.iva = this.factura.subtotal * 0.21;
      this.factura.totalPagar = this.factura.subtotal + this.factura.iva;
      this.itemsList.push(fg.value);
      fg.patchValue({ idItems: fg.value.id });
      this.toastr.success('Guardado Correctamente!', 'Registro!');
    } else {
      fg.value.idItems = i;
      this.factura.subtotal += parseFloat(fg.value.total) - this.itemsList[i].total;
      this.factura.iva = this.factura.subtotal * 0.21;
      this.factura.totalPagar = this.factura.subtotal + this.factura.iva;

      this.itemsList[i] = fg.value;
      this.toastr.info('Actualizado Correctamente!', 'Registro!');
    }
  }

  onDelete(idItems, i) {
    if (confirm('Estás seguro de eliminar este registro?')){
      this.itemFacturaForms.removeAt(i);
      this.factura.subtotal -= this.itemsList[i].total
      this.factura.iva = this.factura.subtotal * 0.21;
      this.factura.totalPagar = this.factura.subtotal + this.factura.iva;
      this.itemsList.splice(i, 1);
      this.toastr.info('Eliminado Correctamente!', 'Registro!');
    }
  }

  updateTotal(fg) {
    fg.patchValue({ total: parseFloat((fg.value.cantidad * fg.value.precio).toFixed(2)) });
  }

  changeSubtotal() {
    this.itemsList.forEach(element => console.log(element.total));
  }
  maskDinero(numeroInt) {
    return parseFloat(numeroInt).toFixed(2).replace(/\D/g, "")
      .replace(/([0-9])([0-9]{2})$/, '$1,$2')
      .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".")+'€';
  }
  formatoFecha(fecha){
    return fecha.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
  }
  generarFactura(opcion: number) {
    const documentDefinition = {

      content: [
        {
          text: 'FACTURA',
          style: 'header'
        },
        {
          columns: [
            {
              text: 'Número de factura',
              style: 'columnDataNumeroFacturaTitle'
            },
            {
              text: 'Fecha de factura:',
              style: 'columnDataNumeroFacturaTitle'
            }
          ]
        },
        {
          columns: [
            {
              text: this.factura.numeroFactura,
              style: 'columnDataNumeroFactura'
            },
            {
              text: this.formatoFecha(this.factura.fechaFactura),
              style: 'columnDataNumeroFactura'
            }

          ]
        },
        {
          columns: [
            {
              text: 'Factura a',
              style: 'columnaDatosEmpresaClienteTitle'
            },
            {
              text: this.factura.nombreEmpresa,
              style: 'columnaDatosEmpresaClienteTitle'
            }
          ]
        },
        {
          columns: [
            {
              text: this.factura.cliente.nombreCliente,
              style: 'columnaDatosEmpresaCliente'
            },
            {
              text: this.factura.direccion + ', ' + this.factura.codigoPostal + ', ' + this.factura.ciudad + ', ' + this.factura.pais,
              style: 'columnaDatosEmpresaCliente'
            }
          ]
        },
        {
          columns: [
            {
              text: this.factura.cliente.direccion,
              style: 'columnaDatosEmpresaCliente'
            },
            {
              text: this.factura.telefono,
              style: 'columnaDatosEmpresaCliente'
            }
          ]
        },
        {
          columns: [
            {
              text: this.factura.cliente.codigoPostal,
              style: 'columnaDatosEmpresaCliente'
            },
            {
              text: this.factura.emailEmpresa,
              style: 'columnaDatosEmpresaCliente'
            }
          ]
        },
        {
          columns: [
            {
              text: this.factura.cliente.ciudad + ', ' + this.factura.cliente.pais,
              style: 'columnDataNumeroFactura'
            },
            {
              text: this.factura.webEmpresa,
              style: 'columnDataNumeroFactura'
            }
          ]
        },
        {
          style: 'section',
          table: {
            widths: [220, 80, 80, 80],
            body: [
              [{
                text: 'Descripción',
                bold: true,
                fontSize: 12
              },
              {
                text: 'Precio ud.',
                style: 'tableHeader'
              },
              {
                text: 'Cantidad',
                style: 'tableHeader'
              },
              {
                text: 'Importe',
                style: 'tableHeader'
              },
              ],
              ...this.itemsList.map(pr => {
                return [
                  { text: pr.descripcion, alignment: 'left', margin: [0, 5, 0, 5], fontSize: 10 },
                  { text: this.maskDinero(pr.precio), alignment: 'right', margin: [0, 5, 0, 5], fontSize: 10 },
                  { text: pr.cantidad, alignment: 'right', margin: [0, 5, 0, 5], fontSize: 10 },
                  { text: this.maskDinero(pr.total), alignment: 'right', margin: [0, 5, 0, 5], fontSize: 10 }
                ];
              })
            ]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 1 || i === node.table.body.length) ? 0.5 : 0;
            },
            vLineWidth: function (i, node) {
              return 0;
            }

          }
        },
        {
          style: 'tablaTotal',
          table: {
            body: [
              [{ text: 'Subtotal', fontSize: 10 }, { text: this.maskDinero(this.factura.subtotal), fontSize: 10 }],
              [{ text: 'IVA %21 ', fontSize: 10 }, { text: this.maskDinero(this.factura.iva), fontSize: 10 }],
              [{ text: 'Total a pagar', bold: true, fontSize: 16 }, { text: this.maskDinero(this.factura.totalPagar), fontSize: 16, bold: true }]
            ]
          },
          layout: {

            paddingLeft: function (i, node) { return 5; },
            paddingRight: function (i, node) { return 5; },
            paddingTop: function (i, node) { return 5; },
            paddingBottom: function (i, node) { return 5; }
          }
        },
      ],
      styles: {
        tableHeader: {
          bold: true,
          fontSize: 12,
          alignment: 'right'
        },
        header: {
          fontSize: 54,
          decorationStyle: 'solid',
          margin: [0, 0, 0, 20],
          width: 200
        },
        columnDataNumeroFacturaTitle: {
          bold: true,
          width: 200
        },
        columnDataNumeroFactura: {
          margin: [0, 0, 0, 30],
          width: 200,
          fontSize: 10
        },
        columnaDatosEmpresaClienteTitle: {
          bold: true,
          width: 200
        },
        columnaDatosEmpresaCliente: {
          width: 200,
          fontSize: 10
        },
        tablaTotal: {
          margin: [300, 30, 0, 0],
          alignment: 'right'
        }
      }
    };
    if (opcion === 1) {
      if (this.factura.numeroFactura == null || this.factura.numeroFactura == '' && this.factura.fechaFactura == null || this.factura.fechaFactura == '') {
        this.toastr.error('Hay campos vacíos!', 'Generar Factura!');
      } else {
        pdfMake.createPdf(documentDefinition).open();
      }
    }
    if (opcion === 2) {
      if (this.factura.numeroFactura == null || this.factura.numeroFactura == '' && this.factura.fechaFactura == null || this.factura.fechaFactura == '') {
        this.toastr.error('Hay campos vacíos!', 'Imprimir Factura!');
      } else {
        pdfMake.createPdf(documentDefinition).print();
      }
    }
    if (opcion === 3) {
      if (this.factura.numeroFactura == null || this.factura.numeroFactura == '' && this.factura.fechaFactura == null || this.factura.fechaFactura == '') {
        this.toastr.error('Hay campos vacíos!', 'Descargar Factura!');
      } else {
        pdfMake.createPdf(documentDefinition).download(this.factura.numeroFactura);
      }
    }
  }

}
